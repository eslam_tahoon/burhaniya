// Initialize your app
var myApp = new Framework7({
	precompileTemplates: false,
	pushState: true,
	cache:false,
	swipeBackPage:false,
	modalButtonCancel:'إلغاء',
	modalButtonOk:'موافق'
});


// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

//- With callbacks on click
 

/* ========== Sponsors ============== 

$$(document).on('click', '.pdf01', function () {
	SitewaertsDocumentViewer.viewDocument(
            'pdf/conf-booklet.pdf','application/pdf')
});
$$(document).on('click', '.pdf02', function () {
	SitewaertsDocumentViewer.viewDocument(
            'pdf/kau-booklet.pdf','application/pdf');
});
$$(document).on('click', '.pres01', function () {
	SitewaertsDocumentViewer.viewDocument(
            'presentations/oral_pres.pdf','application/pdf');
});
$$(document).on('click', '.pres02', function () {
	SitewaertsDocumentViewer.viewDocument(
            'presentations/poster_pres.pdf','application/pdf');
});*/

$$(document).on('click', '.open-pdf-book', function () {
	SitewaertsDocumentViewer.viewDocument(
            'pdf/book.pdf','application/pdf',
				{
					title: 'الآتي',
					documentView : {
						closeLabel : 'عودة'
					},
					navigationView : {
						closeLabel : 'عودة'
					},
					email : {
						enabled : true
					},
					print : {
						enabled : false
					},
					openWith : {
						enabled : false
					},
					bookmarks : {
						enabled : false
					},
					search : {
						enabled : true
					},
				}
			);
});

$$(document).on('click', '.open-pdf-map', function () {
	SitewaertsDocumentViewer.viewDocument(
            'pdf/bookmap.pdf','application/pdf',
				{
					title: 'خريطة الآتي',
					documentView : {
						closeLabel : 'عودة'
					},
					navigationView : {
						closeLabel : 'عودة'
					},
					email : {
						enabled : true
					},
					print : {
						enabled : false
					},
					openWith : {
						enabled : false
					},
					bookmarks : {
						enabled : false
					},
					search : {
						enabled : true
					},
				}
			);
});


// Handle the back button
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
	document.addEventListener("backbutton", onBackKeyDown, false);
}
function onBackKeyDown() {
	mainView.router.back();
}

var fontSize = $$('body').css('font-size').split('px')[0];
myApp.onPageInit('page01', function (page) {
    // run createContentPage func after link was clicked
    $$("body p").css({'font-size':fontSize+'px'});
});
$$(document).on('click', '.settings-btn', function () {
	var buttons = [
        {
            text: 'تكبير الخط',
            onClick: function () {
                /*myApp.alert('تكبير الخط');*/
				if(fontSize>=40){myApp.alert('تم تكبير الخط لأكبر حجم ممكن','خطأ');}
				else{
					fontSize = parseInt(fontSize) + 2;
					$$("body p").css({'font-size':fontSize+'px'});
				}
            }
        },
        {
            text: 'تصغير الخط',
            onClick: function () {
                /*myApp.alert('تصغير الخط');*/
				if(fontSize<=10){myApp.alert('تم تصغيير الخط لأصغر حجم ممكن','خطأ','موافق');}
				else{
					fontSize = parseInt(fontSize) - 2;
					$$("body p").css({'font-size':fontSize+'px'});
				}
            }
        },
        {
            text: 'تعين الحجم اللإفتراضي',
            onClick: function () {
                /*myApp.alert(parseInt(fontSize));*/
				fontSize = 14;
				$$("body p").css({'font-size':fontSize+'px'});
            }
        },
        {
            text: 'إلغاء',
            color: 'red',
            onClick: function () {
                /*myApp.alert('إلغاء');*/
            }
        },
    ];
    myApp.actions(buttons);
});
myApp.onPageInit('page02', function (page) {

	$$('.open-book-gallery').on('click', function () {
		var BookPhotoPopup = myApp.photoBrowser({
			photos: BookPhotos,
			theme: 'light',
			
			ofText:'من',
			type: 'page',
			backLinkText:'<span>عودة</span><i class="fa fa-chevron-left"></i>',
			navbarTemplate:
'<div class="navbar">'+
    '<div class="navbar-inner">'+
       ' <div class="left sliding">'
            +'<span class="photo-browser-current bold"></span> '
            +'<span class="photo-browser-of">{{ofText}}</span> '
            +'<span class="photo-browser-total bold"></span>&nbsp;&nbsp;'
        +'</div>'
		+'<div class="center sliding">كتاب الأوراد</div>'
        +'<div class="right sliding">&nbsp;&nbsp;'

            +'<a href="#" class="link close-popup photo-browser-close-link {{#unless backLinkText}}icon-only{{/unless}} {{js "this.type === \'page\' ? \'back\' : \'\'"}}">'
                
                +'{{#if backLinkText}}<span>{{backLinkText}}</span>{{/if}}'
            +'</a>'
        +'</div>'
    +'</div>'
+'</div>'
			
		});
		BookPhotoPopup.open();
	});
});

/* --- photo_albums --- */
myApp.onPageInit('photo_albums', function (page) {

	$$('.album_trigger').on('click', function () {
		var AlbumPhotoPopup = null;
		AlbumPhotoPopup = myApp.photoBrowser({
			photos: AlbumsPhotos[$$(this).attr("album")],
			theme: 'light',
			
			ofText:'من',
			type: 'page',
			backLinkText:'<span>عودة</span><i class="fa fa-chevron-left"></i>',
			navbarTemplate:
'<div class="navbar">'+
    '<div class="navbar-inner">'+
       ' <div class="left sliding">'
            +'<span class="photo-browser-current bold"></span> '
            +'<span class="photo-browser-of">{{ofText}}</span> '
            +'<span class="photo-browser-total bold"></span>&nbsp;&nbsp;'
        +'</div>'
		+'<div class="center sliding">'+$$(this).find(".item-title").text()+'</div>'
        +'<div class="right sliding">&nbsp;&nbsp;'

            +'<a href="#" class="link close-popup photo-browser-close-link {{#unless backLinkText}}icon-only{{/unless}} {{js "this.type === \'page\' ? \'back\' : \'\'"}}">'
                
                +'{{#if backLinkText}}<span>{{backLinkText}}</span>{{/if}}'
            +'</a>'
        +'</div>'
    +'</div>'
+'</div>'
			
		});
		AlbumPhotoPopup.open();
	});
});


myApp.onPageInit('books', function (page) {
	
	$$('.pdf_trigger').on('click', function () {
		if(myApp.device.os=='android'){
			asset2sd.copyDir({
				asset_directory:"www/pdf",
				destination_directory:"Burhaniya"},
				function(list) {
					cordova.plugins.fileOpener2.open(
						cordova.file.externalRootDirectory+'Burhaniya/'+$$(this).attr("pdf"), // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Download/starwars.pdf
						'application/pdf', 
						{ 
							error : function(e) { 
								console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
								myApp.confirm('هل تريد تحميل تطبيق لعرض الكتاب', 'لا يوجد تطبيق لعرض الكتيب', function () {
									window.open('market://details?com.adobe.reader','_system');
									
								});
							},
							success : function () {
								console.log('file opened successfully');                
							}
						}
					);
				},
				function() { 
					myApp.hideIndicator();
					myApp.alert('لا يمكن فتح الكتاب .. رجاء تاكد من صلاحيات الجهاز','خطأ'); 
				}
			); 
		}else{
			SitewaertsDocumentViewer.viewDocument(
					'pdf/'+$$(this).attr("pdf"),'application/pdf',
						{
							title: $$(this).find(".item-title").text(),
							documentView : {
								closeLabel : 'عودة'
							},
							navigationView : {
								closeLabel : 'عودة'
							},
							email : {
								enabled : true
							},
							print : {
								enabled : false
							},
							openWith : {
								enabled : false
							},
							bookmarks : {
								enabled : false
							},
							search : {
								enabled : true
							},
						}
					);
		}
	});
});